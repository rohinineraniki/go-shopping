const myform = document.getElementById("my-form");
const firstName = document.getElementById("first-name");
const emptyFirstName = document.getElementById("empty-first-name");

const lastName = document.getElementById("last-name");
const emptyLastName = document.getElementById("empty-last-name");

const email = document.getElementById("email");
const emptyEmail = document.getElementById("empty-email");

const password = document.getElementById("password");
const emptyPassword = document.getElementById("empty-password");

const confirmPassword = document.getElementById("confirm-password");
const emptyConfirmPassword = document.getElementById("empty-confirm-password");

const successMsg = document.getElementById("success-msg");

const goBack = document.getElementById("go-back");
const login = document.getElementById("login");

const buttonContainer = document.getElementById("button-container");
const logoutContainer = document.getElementById("logout-container");

const userText = document.getElementById("user-text");
const logout = document.getElementById("logout");

// logoutContainer.classList.add("no-display");

goBack.addEventListener("click", function (event) {
  document.location.href = "index.html";
});

logout.addEventListener("click", function (event) {
  localStorage.removeItem("newUser");
  buttonContainer.classList.remove("no-display");
  logoutContainer.classList.add("no-display");
});

const getUser = localStorage.getItem("newUser");
const parsedGetUser = JSON.parse(getUser);

if (parsedGetUser !== null) {
  console.log(parsedGetUser);
  buttonContainer.classList.add("no-display");
  logoutContainer.classList.remove("no-display");
  console.log(parsedGetUser.firstName);
  userText.textContent = `Welcome ${parsedGetUser.firstName} ${parsedGetUser.lastName}`;
} else {
  buttonContainer.classList.remove("no-display");
  logoutContainer.classList.add("no-display");
}

const newUser = {
  id: 1,
  firstName: "",
  lastName: "",
  mail: "",
};

function isAlphabet(char) {
  let charArray = String(char).split("");
  let charArrayResult = charArray.filter((each) => {
    return (
      typeof each === "string" &&
      each.length === 1 &&
      ((each >= "a" && each <= "z") || (each >= "A" && each <= "Z"))
    );
  });
  return charArrayResult.length === char.length;
}

myform.addEventListener("submit", function (event) {
  event.preventDefault();

  let inputFirstName = String(firstName.value);
  let checkFirstName = isAlphabet(inputFirstName);

  let inputLastName = String(lastName.value);
  let checkLastName = isAlphabet(inputLastName);

  let inputPassword = password.value;
  let inputConfirmPassword = confirmPassword.value;

  if (!checkFirstName) {
    emptyFirstName.textContent = "Please enter valid name";
  } else if (!checkLastName) {
    emptyLastName.textContent = "Please enter valid name";
  } else if (inputPassword.length < 8) {
    emptyPassword.textContent =
      "Password must be greater than or equal to 8 characters";
  } else if (inputPassword !== inputConfirmPassword) {
    emptyConfirmPassword.textContent =
      "Password and confirm password must be same";
  } else {
    successMsg.textContent =
      "Registration completed successfully, you will redirect back to home page !!";
    console.log(firstName.value);
    newUser.firstName = firstName.value;
    newUser.lastName = lastName.value;
    newUser.mail = email.value;

    localStorage.setItem("newUser", JSON.stringify(newUser));
    myform.reset();
    setTimeout(() => {
      document.location.href = "index.html";
    }, 3000);
  }
});

firstName.addEventListener("blur", function (event) {
  if (event.target.value === "") {
    emptyFirstName.textContent = "Required*";
  } else {
    emptyFirstName.textContent = "";
  }
});

lastName.addEventListener("blur", function (event) {
  console.log(event);
  if (event.target.value === "") {
    emptyLastName.textContent = "Required*";
  } else {
    emptyLastName.textContent = "";
  }
});

email.addEventListener("blur", function (event) {
  if (event.target.value === "") {
    emptyEmail.textContent = "Required*";
  } else {
    emptyEmail.textContent = "";
  }
});

password.addEventListener("blur", function (event) {
  if (event.target.value === "") {
    emptyPassword.textContent = "Required*";
  } else {
    emptyPassword.textContent = "";
  }
});

confirmPassword.addEventListener("blur", function (event) {
  if (event.target.value === "") {
    emptyConfirmPassword.textContent = "Required*";
  } else {
    emptyConfirmPassword.textContent = "";
  }
});
