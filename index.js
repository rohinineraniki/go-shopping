const mainContainer = document.getElementById("main-container");
const singleProduct = document.getElementById("single-product");
const loader = document.getElementById("loader");
const signup = document.getElementById("signup");

const login = document.getElementById("login");

const buttonContainer = document.getElementById("button-container");
const logoutContainer = document.getElementById("logout-container");

const userText = document.getElementById("user-text");
const logout = document.getElementById("logout");
const goBack = document.getElementById("go-back");

// logoutContainer.classList.add("no-display");

signup.addEventListener("click", function (event) {
  document.location.href = "signup.html";
});

goBack.addEventListener("click", function (event) {
  document.location.href = "index.html";
});

logout.addEventListener("click", function (event) {
  localStorage.removeItem("newUser");
  buttonContainer.classList.remove("no-display");
  logoutContainer.classList.add("no-display");
});

const getUser = localStorage.getItem("newUser");
const parsedGetUser = JSON.parse(getUser);

if (parsedGetUser !== null) {
  console.log(parsedGetUser);
  buttonContainer.classList.add("no-display");
  logoutContainer.classList.remove("no-display");
  console.log(parsedGetUser.firstName);
  userText.textContent = `Welcome ${parsedGetUser.firstName} ${parsedGetUser.lastName}`;
} else {
  buttonContainer.classList.remove("no-display");
  logoutContainer.classList.add("no-display");
}

const getProductsData = function (url) {
  loader.style.display = "flex";
  return new Promise((resolve, reject) => {
    fetch(url)
      .then((res) => {
        return res.json();
      })
      .then((data) => {
        resolve(data);
      })
      .catch((error) => {
        reject(error);
      });
  });
};

const onclickGoback = () => {
  singleProduct.style.display = "none";
  mainContainer.style.display = "flex";
  singleProduct.textContent = "";
};

const appendProduct = (product) => {
  const { id, title, price, description, category, image, rating } = product;

  let productContainer = document.createElement("div");
  productContainer.classList.add("product-container");
  // mainContainer.appendChild(productContainer);

  let productImage = document.createElement("img");
  productImage.setAttribute("src", image);
  productImage.classList.add("product-image");
  productImage.setAttribute("id", id);

  productImage.addEventListener("click", onclickProduct);
  productContainer.appendChild(productImage);

  let titleContainer = document.createElement("div");
  titleContainer.classList.add("title-container");
  productContainer.appendChild(titleContainer);

  let titleHeading = document.createElement("h3");
  titleHeading.textContent = title;
  titleContainer.appendChild(titleHeading);

  let costContainer = document.createElement("div");
  costContainer.classList.add("cost-container");
  titleContainer.appendChild(costContainer);

  let rupeeIcon = document.createElement("i");
  rupeeIcon.classList.add("fa", "fa-indian-rupee-sign");
  rupeeIcon.classList.add("rupee-icon");
  costContainer.appendChild(rupeeIcon);

  let priceHeading = document.createElement("h1");
  priceHeading.textContent = price;
  costContainer.appendChild(priceHeading);

  return productContainer;
};

const appendSingleProduct = (data) => {
  const { id, title, price, description, category, image, rating } = data;
  let { rate, count } = rating;
  singleProduct.textContent = "";

  let singleProductContainer = document.createElement("div");
  singleProductContainer.classList.add("single-product-container");
  singleProduct.appendChild(singleProductContainer);

  let singleProductImage = document.createElement("img");
  singleProductImage.classList.add("single-product-image");
  singleProductImage.setAttribute("src", image);
  singleProductContainer.appendChild(singleProductImage);

  let aboutProductContainer = document.createElement("div");
  aboutProductContainer.classList.add("about-product");
  singleProductContainer.appendChild(aboutProductContainer);

  let titleHeading = document.createElement("h2");
  titleHeading.textContent = title;
  aboutProductContainer.appendChild(titleHeading);

  let productPara = document.createElement("p");
  productPara.textContent = description;
  productPara.classList.add("para");
  aboutProductContainer.appendChild(productPara);

  let ratingContainer = document.createElement("div");
  ratingContainer.classList.add("rating-container");
  aboutProductContainer.appendChild(ratingContainer);

  let starIcon = document.createElement("i");
  starIcon.classList.add("fa-solid", "fa-star", "fa-lg", "star-icon");
  starIcon.style.color = "#e0e316";
  ratingContainer.appendChild(starIcon);

  let ratingPara = document.createElement("p");
  ratingPara.textContent = rate;
  ratingPara.classList.add("rating-para");
  ratingContainer.appendChild(ratingPara);

  let countPara = document.createElement("p");
  countPara.textContent = `Total ratings ${count}`;
  countPara.classList.add("para");
  aboutProductContainer.appendChild(countPara);

  let categoryPara = document.createElement("p");
  categoryPara.textContent = `Product Catagory ${category}`;
  countPara.classList.add("para");
  aboutProductContainer.appendChild(countPara);

  let costContainer = document.createElement("div");
  costContainer.classList.add("cost-container");
  aboutProductContainer.appendChild(costContainer);

  let rupeeIcon = document.createElement("i");
  rupeeIcon.classList.add("fa", "fa-indian-rupee-sign", "rupee-icon");
  costContainer.appendChild(rupeeIcon);

  let priceHeading = document.createElement("h1");
  priceHeading.textContent = price;
  costContainer.appendChild(priceHeading);

  let gobackButton = document.createElement("button");
  gobackButton.textContent = "Go Back";
  gobackButton.addEventListener("click", onclickGoback);
  aboutProductContainer.appendChild(gobackButton);
};

const onclickProduct = (event) => {
  let clickId = event.target.id;
  console.log("the clickid", clickId);
  mainContainer.style.display = "none";
  singleProduct.style.display = "block";
  let url = `https://fakestoreapi.com/products/${clickId}`;
  getProductsData(url)
    .then((data) => {
      loader.style.display = "none";
      appendSingleProduct(data);
    })
    .catch((error) => {
      loader.style.display = "none";
      let errorMsg = document.createElement("h1");
      errorMsg.classList.add("error-msg");
      errorMsg.textContent =
        "Internal server error, Please try after some time";
      singleProduct.appendChild(errorMsg);
      console.log(error.message);
      console.error(error);
    });
};

const displayProducts = (data) => {
  const displayAllProducts = data.map((product) => {
    let eachProduct = appendProduct(product);
    return eachProduct;
  });

  mainContainer.append(...displayAllProducts);
};

getProductsData("https://fakestoreapi.com/products/")
  .then((data) => {
    loader.style.display = "none";
    displayProducts(data);
  })
  .catch((error) => {
    loader.style.display = "none";
    let errorMsg = document.createElement("h1");
    errorMsg.classList.add("error-msg");
    errorMsg.textContent = "Internal server error, Please try after some time";
    mainContainer.appendChild(errorMsg);
    console.error(error);
  });
